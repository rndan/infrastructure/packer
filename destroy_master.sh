#!/bin/bash
ami_id=$(grep 'artifact,0,id' ./packer/build.log | cut -d, -f6 | cut -d: -f2)
cd terraform
terraform destroy -auto-approve -var="ami_id=$ami_id"
aws ec2 describe-images --image-ids $ami_id > describe_images.log
snapshot_id=$(jq -r '.Images[].BlockDeviceMappings[].Ebs.SnapshotId | select(. != null)' "describe_images.log")
aws ec2 deregister-image --image-id $ami_id
aws ec2 delete-snapshot --snapshot-id $snapshot_id
rm describe_images.log
echo "Image and snapshot deleted"

