variable "ami_id" {
  description = "The ID of the AMI to use for the instance"
  type        = string
}

variable "ssh_key" {
    description = "Name of the key that will be used to communicate with the ec2 instance"
    type        = string
}

variable "volume_size" {
    description = "Size of the ssd disk attached to the ec2 instance in GB"
    type        = number
    default     = 20
}

