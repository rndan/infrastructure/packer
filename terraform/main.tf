provider "aws" {
  region = "eu-central-1"
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_security_group" "allow_all" {
  name        = "allow_all_traffic"
  description = "Security group with all inbound and outbound traffic allowed"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "master" {
  ami                 = var.ami_id
  instance_type       = "t2.medium"
  vpc_security_group_ids = [aws_security_group.allow_all.id]
  key_name            = var.ssh_key

  root_block_device {
    volume_type = "gp2"
    volume_size = var.volume_size
  }

  tags = {
    Name = "master-instance"
  }
}

output "public_ip" {
  value = aws_instance.master.public_ip
}

