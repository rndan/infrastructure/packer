#!/bin/bash

usage() {
  echo "Usage: $0 <key name>"
  exit 1
}

if [ -z "$1" ]; then
  echo "Error: No argument provided."
  usage
fi

cd packer
packer init aws-ubuntu.pkr.hcl
packer build -machine-readable . | tee build.log
ami_id=$(grep 'artifact,0,id' build.log | cut -d, -f6 | cut -d: -f2)

cd ../terraform
terraform init
terraform apply -var "ami_id=$ami_id" -var "ssh_key=$1" -auto-approve

