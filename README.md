# Overview
Packer and Terraform code for dynamically creating AMI using Ansible and EC2 instance needed to create cloud-core part for KubeEdge deployment
### Requirements:
* AWS Account
* AWS CLI installed and configured properly
* Packer
* Terraform

### How to use:
* For creating EC2 instance and AMI on your AWS account run "create_master.sh" script.
* For deleting EC2 instance, AMI and associated snapshot record run "destroy_master.sh" script.

*IMPORTANT: These scripts are not designed for "safe rerun" and should be used with caution.*
